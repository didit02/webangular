import {Injectable} from '@angular/core';
import {Http, Headers} from '@angular/http';
import {map} from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})

export class DataserviceService {

    LoggedStatus = false;
    useridget = '';

    constructor(private http: Http) {
    }

    setLoggedin(value: boolean) {
        this.LoggedStatus = value;
    }

    get isLoggedIn() {
        return this.LoggedStatus;
    }

    sendRegister(email, no_hp, password) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        let data = {email: email, no_hp: no_hp, password: password};
        let json = JSON.stringify(data);
        let url = 'http://localhost:3000/registerakun';
        let getData = this.http.post(url, json, {
            headers: header
        });
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    sendLogin(email, sandi) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        let data = {email: email, password: sandi};
        let json = JSON.stringify(data);
        let url = 'http://localhost:3000/login';
        let getData = this.http.post(url, json, {
            headers: header
        });
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    saveuserId(value: string) {
        this.useridget = value;
    }

    get userid() {
        return this.useridget;
    }

    uploadStory(user_id, category_id, story, date, status_id) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        let data = {user_id: user_id, category_id: category_id, story: story, date: date, status_id: status_id};
        let json = JSON.stringify(data);
        let url = 'http://localhost:3000/uploadstory';
        let getData = this.http.post(url, json, {
            headers: header
        });
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    readStory() {
        let url = 'http://localhost:3000/loadallstory';
        let getData = this.http.get(url);
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    getuserDetail(idloggeduser) {
        let url = 'http://localhost:3000/getuserbyid/id=' + idloggeduser;
        let getData = this.http.get(url);
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    searchStory(keyword) {
        let url = 'http://localhost:3000/searchstory/search=' + keyword;
        let getData = this.http.get(url);
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    getCategory() {
        let url = 'http://localhost:3000/category';
        let getData = this.http.get(url);
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    getStatus() {
        let url = 'http://localhost:3000/status';
        let getData = this.http.get(url);
        return getData.pipe(map(res => {
            return res.json()
        }))
    }


    mystory(id) {
        let url = 'http://localhost:3000/getstorybyid/id=' + id;
        let getData = this.http.get(url);
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    complaint(email, no_hp, complaint) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        let data = {email: email, no_hp: no_hp, complaint: complaint};
        let json = JSON.stringify(data);
        let url = 'http://localhost:3000/complaint';
        let getData = this.http.post(url, json, {
            headers: header
        });
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    uploadimageuser(image) {
        let header = new Headers();
        let url = 'http://localhost:3000/imageUser';
        let getData = this.http.post(url, image, {
            headers: header
        });
        return getData.pipe(map(res => {
            return res
        }))
    }

    uploadImgaDb(image, iduser) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        let data = {image: image, iduser: iduser};
        let json = JSON.stringify(data);
        let url = 'http://localhost:3000/saveImageUser';
        let getData = this.http.post(url, json, {
            headers: header
        });
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    getImageUser(id) {
        let url = 'http://localhost:3000/getImageUser/id=' + id;
        let getData = this.http.get(url);
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    changePassword(password, iduser) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        let data = {password: password, iduser: iduser};
        let json = JSON.stringify(data);
        let url = 'http://localhost:3000/changepassword';
        let getData = this.http.post(url, json, {
            headers: header
        });
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    filterCategory(category) {
        let url = 'http://localhost:3000/filterstory/category=' + category;
        let getData = this.http.get(url);
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    uploadImgaStoryDb(user_id, image , story_id) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        let data = {user_id:user_id , image: image , story_id:story_id};
        let json = JSON.stringify(data);
        let url = 'http://localhost:3000/uploadimagestory';
        let getData = this.http.post(url, json, {
            headers: header
        });
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    createuserSerivce(nama_jasa, no_hp_jasa , alamat , user_id , image ) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        let data = {nama_jasa:nama_jasa , no_hp_jasa: no_hp_jasa , alamat:alamat , user_id:user_id , image:image};
        let json = JSON.stringify(data);
        let url = 'http://localhost:3000/createaccountservice';
        let getData = this.http.post(url, json, {
            headers: header
        });
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    accountuserservice(user_id) {
        let url = 'http://localhost:3000/getacountuserservice/user_id=' + user_id;
        let getData = this.http.get(url);
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    useruploadLowongan(category_id, status_id , deskripsi_lowongan , bayaran , id_userpunyagawai , date) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        let data = {category_id:category_id , status_id: status_id , deskripsi_lowongan:deskripsi_lowongan , bayaran:bayaran , id_userpunyagawai:id_userpunyagawai , date:date};
        let json = JSON.stringify(data);
        let url = 'http://localhost:3000/uploadlowongan';
        let getData = this.http.post(url, json, {
            headers: header
        });
        return getData.pipe(map(res => {
            return res.json()
        }))
    }
    uploadImgaLowonganDb(id_userpunyagawai, image , lowonganupload_id) {
        let header = new Headers();
        header.append('Content-Type', 'application/json');
        let data = {id_userpunyagawai:id_userpunyagawai , image: image , lowonganupload_id:lowonganupload_id};
        let json = JSON.stringify(data);
        let url = 'http://localhost:3000/uploadimagelowongan';
        let getData = this.http.post(url, json, {
            headers: header
        });
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    getalllowongan() {
        let url = 'http://localhost:3000/getalllowongan';
        let getData = this.http.get(url);
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    getalllowonganbyid(id) {
        let url = 'http://localhost:3000/getalllowonganbyid/id=' + id;
        let getData = this.http.get(url);
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    getpunyagawaiDetail(idpunyagawai) {
        let url = 'http://localhost:3000/getuserpunyagawaibyid/id=' + idpunyagawai;
        let getData = this.http.get(url);
        return getData.pipe(map(res => {
            return res.json()
        }))
    }

    getdetaillowonganbyid(id) {
        let url = 'http://localhost:3000/detailgawaibyid/id=' + id;
        let getData = this.http.get(url);
        return getData.pipe(map(res => {
            return res.json()
        }))
    }
}

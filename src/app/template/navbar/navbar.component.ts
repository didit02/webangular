import {Component, OnInit, Input} from '@angular/core';
import {Router} from "@angular/router";

@Component({
    selector: 'app-navbar', templateUrl: './navbar.component.html', styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
    logged: boolean;
    beranda: boolean;
    tentang: boolean;
    trigerexpand:boolean;

    constructor(private router: Router) {
    }

    ngOnInit() {
        this.beranda = true;
        this.tentang = false;
        this.trigerexpand = false;
        if (this.router.url === '/about'){
            this.tentang = true;
            this.beranda = false;
        }else if (this.router.url === '/home'){
            this.tentang = false;
            this.beranda = true;
        }

        const emaillocalStorage = localStorage.getItem('email');
        this.logged = emaillocalStorage !== null ? false : true;
    }


    home() {
        this.router.navigateByUrl('/home');
        this.beranda = true;
        this.tentang = false;
        this.trigerexpand = false;
    }

    about() {
        this.router.navigateByUrl('/about');
        this.beranda = false;
        this.tentang = true;
        this.trigerexpand = false;
    }

    help() {
        this.trigerexpand = this.trigerexpand == false;
    }
    gawai(){
        this.router.navigateByUrl('/gawai');
    }

}

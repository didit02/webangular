import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {DataserviceService} from "./service/dataservice.service";
import {ShareserviceService} from "./share/shareservice.service";
import {HttpModule } from '@angular/http';
import {AuthGuard} from "./auth.guard";
import {ReactiveFormsModule, FormsModule} from "@angular/forms";


import {AppComponent} from './app.component';
import {AppRoutingModule} from './/app-routing.module';
import {HeaderComponent} from './template/header/header.component';
import {NavbarComponent} from './template/navbar/navbar.component';
import {FooterComponent} from './template/footer/footer.component';
import {MenuComponent} from './template/menu/menu.component';
import {SidebarComponent} from './template/sidebar/sidebar.component';
import {LoginComponent} from './views/pages/login/login.component';
import {SignupComponent} from './views/pages/signup/signup.component';
import {HomeComponent} from './views/pages/home/home.component';
import {SearchComponent} from './views/widget/search/search.component';
import {AboutComponent} from './views/pages/about/about.component';
import {HelpComponent} from './views/pages/help/help.component';
import {ProfileUserComponent} from './views/content/profile-user/profile-user.component';
import {EditUserComponent} from './views/pages/edit-user/edit-user.component';
import {CreateStoryComponent} from './views/pages/create-story/create-story.component';
import {ExpandHelpComponent} from './views/widget/expand-help/expand-help.component';
import {ContactComponent} from './views/pages/contact/contact.component';
import {HowShareComponent} from './views/pages/how-share/how-share.component';
import {MyStoryComponent} from './views/pages/my-story/my-story.component';
import { DashboardUserComponent } from './views/pages-private/dashboard-user/dashboard-user.component';
import {NavbarUserComponent} from "./views/widget/navbar-user/navbar-user.component";
import { HistoryAccountComponent } from './views/content/history-account/history-account.component';
import { SettingaccountUserComponent } from './views/content/settingaccount-user/settingaccount-user.component';
import { CreateaccountPunyagawaiComponent } from './views/pages/createaccount-punyagawai/createaccount-punyagawai.component';
import {CreateaccountServiceComponent} from "./views/content/createaccount-service/createaccount-service.component";
import {UploadLowonganComponent} from "./views/pages/upload-lowongan/upload-lowongan.component";
import {UserUploadLowonganComponent} from "./views/content/user-upload-lowongan/user-upload-lowongan.component";
import { RiwayatPunyagawaiComponent } from './views/pages/riwayat-punyagawai/riwayat-punyagawai.component';
import { GawaiComponent } from './views/pages/gawai/gawai.component';
import { HistoryPunyagawaiComponent } from './views/content/history-punyagawai/history-punyagawai.component';
import { SettingaccountPunyagawaiComponent } from './views/content/settingaccount-punyagawai/settingaccount-punyagawai.component';
import { EditPunyagawaiUserComponent } from './views/pages/edit-punyagawai-user/edit-punyagawai-user.component';
import { DetailGawaiComponent } from './views/pages/detail-gawai/detail-gawai.component';


@NgModule({
    declarations: [AppComponent, HeaderComponent, NavbarComponent, FooterComponent, MenuComponent, SidebarComponent, LoginComponent, SignupComponent, HomeComponent, SearchComponent, AboutComponent, HelpComponent, ProfileUserComponent, EditUserComponent, CreateStoryComponent, ExpandHelpComponent, ContactComponent, HowShareComponent, MyStoryComponent, DashboardUserComponent , NavbarUserComponent, HistoryAccountComponent, SettingaccountUserComponent, CreateaccountPunyagawaiComponent , CreateaccountServiceComponent, UploadLowonganComponent, UserUploadLowonganComponent, RiwayatPunyagawaiComponent, GawaiComponent, HistoryPunyagawaiComponent, SettingaccountPunyagawaiComponent, EditPunyagawaiUserComponent, DetailGawaiComponent],
    imports: [BrowserModule, FormsModule, AppRoutingModule, HttpModule, ReactiveFormsModule ],
    providers: [DataserviceService, AuthGuard, ShareserviceService],
    bootstrap: [AppComponent]
})
export class AppModule {
}

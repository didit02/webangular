import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import {LoginComponent} from './views/pages/login/login.component';
import {SignupComponent} from './views/pages/signup/signup.component';
import {HomeComponent} from "./views/pages/home/home.component";
import {AboutComponent} from "./views/pages/about/about.component";
import {HowShareComponent} from "./views/pages/how-share/how-share.component";
import {ContactComponent} from "./views/pages/contact/contact.component";
import {DashboardUserComponent} from "./views/pages-private/dashboard-user/dashboard-user.component";
import {GawaiComponent} from "./views/pages/gawai/gawai.component";
import {DetailGawaiComponent} from "./views/pages/detail-gawai/detail-gawai.component";
import {AuthGuard} from "./auth.guard";

const routes: Routes = [
    { path: '', redirectTo: '/home', pathMatch: 'full' },
    { path: 'home', component: HomeComponent},
    { path: 'login', component: LoginComponent },
    { path: 'register', component: SignupComponent },
    { path: 'about', component: AboutComponent },
    { path: 'how_share', component: HowShareComponent},
    { path: 'contact', component: ContactComponent},
    { path: 'dashboard_user', component: DashboardUserComponent , canActivate: [AuthGuard]},
    { path: 'detailgawai/:id', component: DetailGawaiComponent},
    { path: 'gawai', component: GawaiComponent}
];

@NgModule({
    imports: [ RouterModule.forRoot(routes) ],
    exports: [ RouterModule ]
})
export class AppRoutingModule { }

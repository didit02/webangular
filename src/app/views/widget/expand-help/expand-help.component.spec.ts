import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpandHelpComponent } from './expand-help.component';

describe('ExpandHelpComponent', () => {
  let component: ExpandHelpComponent;
  let fixture: ComponentFixture<ExpandHelpComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpandHelpComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpandHelpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit, EventEmitter, Output} from '@angular/core';
import {ShareserviceService} from "../../../share/shareservice.service";

import {DataserviceService} from "../../../service/dataservice.service";


@Component({
    selector: 'app-search', templateUrl: './search.component.html', styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {
    @Output() valueSearch = new EventEmitter();
    datasearch = [];
    category = [];
    datedate:any;
    showsearchalert:boolean;
    showbuttonsearchdalert:boolean;
    constructor(private service: DataserviceService, private share: ShareserviceService) {
    }

    ngOnInit() {
        this.showsearchalert = false;
        this.showbuttonsearchdalert = false;
        this.service.getCategory().subscribe(data => {
            const dataall = data.data;
            for (let i = 0; i < dataall.length; i++) {
                this.category.push(dataall[i])
            }
        });

    }

    search(keyword) {
        if (keyword !== '') {
            this.service.searchStory(keyword).subscribe(data => {
                const dataget = data.data;
                if (dataget.length < 1){
                    this.showsearchalert = true;
                    setTimeout(() => {
                        this.showsearchalert = false;
                    }, 3000);
                }else {
                    for (let i = 0; i < dataget.length; i++) {
                        this.datasearch.push(dataget[i])
                    }
                    this.valueSearch.emit(this.datasearch);
                    this.datasearch = [];
                }
            })
        } else {
            this.showbuttonsearchdalert = true;
            setTimeout(() => {
                this.showbuttonsearchdalert = false;
            }, 3000);
        }
    }

    searchenter(keyword) {
        if (keyword !== '') {
            this.service.searchStory(keyword).subscribe(data => {
                const dataget = data.data;
                if (dataget.length < 1){
                    this.showsearchalert = true;
                    setTimeout(() => {
                        this.showsearchalert = false;
                    }, 3000);
                }else {
                    for (let i = 0; i < dataget.length; i++) {
                        this.datasearch.push(dataget[i])
                    }
                    this.valueSearch.emit(this.datasearch);
                    this.datasearch = [];
                }
            })
        } else {
            this.showbuttonsearchdalert = true;
            setTimeout(() => {
                this.showbuttonsearchdalert = false;
            }, 3000);
        }
    }

    selectOption(value){
        const valueoption = value;
        if (valueoption === 'all'){
            this.service.readStory().subscribe(data => {
                const dataget = data.data;
                if (dataget.length < 1){
                    this.showsearchalert = true;
                    setTimeout(() => {
                        this.showsearchalert = false;
                    }, 3000);
                }else {
                    for (let i = 0; i < dataget.length; i++) {
                        const date = dataget[i].date;
                        const datedb = new Date(date);
                        const today = new Date();
                        const getdatenow = today.setHours(0, 0, 0, 0);
                        const timeDiff = Math.abs(datedb.getTime() - getdatenow);
                        const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                        if (diffDays === 0) {
                            this.datedate = 'hari ini'
                        } else {
                            this.datedate = diffDays + ' hari yang lalu'
                        }
                        dataget[i]['date_convert'] = this.datedate;
                        this.datasearch.push(dataget[i])
                    }
                    this.valueSearch.emit(this.datasearch);
                    this.datasearch = [];
                }
            });
        }else {
            this.service.filterCategory(valueoption).subscribe(data =>{
                const dataget = data.data;
                if (dataget.length < 1){
                    this.showsearchalert = true;
                    setTimeout(() => {
                        this.showsearchalert = false;
                    }, 3000);
                }else {
                    for (let i = 0; i < dataget.length; i++) {
                        const date = dataget[i].date;
                        const datedb = new Date(date);
                        const today = new Date();
                        const getdatenow = today.setHours(0, 0, 0, 0);
                        const timeDiff = Math.abs(datedb.getTime() - getdatenow);
                        const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                        if (diffDays === 0) {
                            this.datedate = 'hari ini'
                        } else {
                            this.datedate = diffDays + ' hari yang lalu'
                        }
                        dataget[i]['date_convert'] = this.datedate;
                        this.datasearch.push(dataget[i])
                    }
                    this.valueSearch.emit(this.datasearch);
                    this.datasearch = [];
                }
            })
        }
    }


}

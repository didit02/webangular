import {Component, EventEmitter, OnInit, Output} from '@angular/core';


@Component({
    selector: 'app-navbar-user',
    templateUrl: './navbar-user.component.html',
    styleUrls: ['./navbar-user.component.scss']
})
export class NavbarUserComponent implements OnInit {
    @Output() trigerContent = new EventEmitter();
    trigercontent:string;
    constructor() {
    }

    ngOnInit() {
    }

    mystoryUser(){
        this.trigerContent.emit('story')
    }
    createstoryUser(){
        this.trigerContent.emit('create_story')
    }
    settingAkun(){
        this.trigerContent.emit('setting_account')
    }
}

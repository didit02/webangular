import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {DataserviceService} from "../../../service/dataservice.service";


@Component({
    selector: 'app-contact', templateUrl: './contact.component.html', styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

    show: boolean;
    showstoryalert: boolean;
    showstoryuploadalert:boolean;
    @ViewChild('email') email: ElementRef;
    @ViewChild('nohp') nohp: ElementRef;
    @ViewChild('text') text: ElementRef;
    constructor(private service: DataserviceService) {
    }

    ngOnInit() {
        this.show = false;
        this.showstoryalert = false;
        this.showstoryuploadalert = false;
    }

    sendComplaint(email, no_hp, complaint) {
        const getemail = email;
        if (this.validateEmail(getemail)) {
            if (complaint !== ''){
                this.service.complaint(email, no_hp, complaint).subscribe(data => {
                    this.email.nativeElement.value = '';
                    this.nohp.nativeElement.value = '';
                    this.text.nativeElement.value = '';
                    this.showstoryuploadalert = true;
                    setTimeout(() => {
                        this.showstoryuploadalert = false;
                    }, 3000);
                });
            }else {
                this.showstoryalert = true;
                setTimeout(() => {
                    this.showstoryalert = false
                }, 3000);
            }
        } else {
            this.show = true;
            setTimeout(() => {
                this.show = false
            }, 3000);
        }
    }

    validateEmail(getemail) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(getemail);
    }

    validateEmailUser(email) {
        const getemail = email;
        if (!this.validateEmail(getemail)) {
            this.show = true;
            setTimeout(() => {
                this.show = false
            }, 3000);
        }
    }
}

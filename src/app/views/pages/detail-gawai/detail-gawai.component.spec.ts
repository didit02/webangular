import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailGawaiComponent } from './detail-gawai.component';

describe('DetailGawaiComponent', () => {
  let component: DetailGawaiComponent;
  let fixture: ComponentFixture<DetailGawaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailGawaiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailGawaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";

import {DataserviceService} from "../../../service/dataservice.service";


@Component({
    selector: 'app-detail-gawai',
    templateUrl: './detail-gawai.component.html',
    styleUrls: ['./detail-gawai.component.scss']
})
export class DetailGawaiComponent implements OnInit {

    id: number;
    name_usaha: string;
    address: string;
    no_tlp: string;
    payment: number;
    deskripsi_lowongan:string;
    constructor(private activatedRoute: ActivatedRoute, private service: DataserviceService) {
    }

    ngOnInit() {
        this.activatedRoute.params.subscribe(paramsId => {
            this.id = paramsId.id;
        });
        this.service.getdetaillowonganbyid(this.id).subscribe(data => {
            const dataall = data.data;
            console.log('data :',dataall);
            this.name_usaha = dataall[0].nama_jasa;
            this.address = dataall[0].alamat;
            this.no_tlp = dataall[0].no_hp_jasa;
            this.payment = dataall[0].bayaran;
            this.deskripsi_lowongan = dataall[0].deskripsi_lowongan;
        });
    }

}

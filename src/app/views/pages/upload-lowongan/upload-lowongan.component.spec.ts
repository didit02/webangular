import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadJasaComponent } from './upload-lowongan.component';

describe('UploadJasaComponent', () => {
  let component: UploadJasaComponent;
  let fixture: ComponentFixture<UploadJasaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadJasaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadJasaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {DataserviceService} from "../../../service/dataservice.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';


@Component({
  selector: 'app-upload-lowngan',
  templateUrl: './upload-lowongan.component.html',
  styleUrls: ['./upload-lowongan.component.scss']
})
export class UploadLowonganComponent implements OnInit {

    form: FormGroup;
    category = [];
    status = [];
    selectRadio: any;
    showstoryalert: boolean;
    showstoryuploadalert: boolean;
    showstatusalert: boolean;
    imageSrc: string;
    iduserpunyagawai:number;
    iduserlocal = localStorage.getItem('iduser');

    @ViewChild('lowongan') lowongan: ElementRef;
    @ViewChild('bayaran') bayaran: ElementRef;

    constructor(private service: DataserviceService , private fb: FormBuilder) {
        this.createForm();
    }


  ngOnInit() {
      this.showstoryalert = false;
      this.showstoryuploadalert = false;
      this.showstatusalert = false;
      this.service.getCategory().subscribe(data => {
          const dataall = data.data;
          for (let i = 0; i < dataall.length; i++) {
              this.category.push(dataall[i]);
          }
      });
      this.service.getStatus().subscribe(data => {
          const dataall = data.data;
          for (let i = 0; i < dataall.length; i++) {
              this.status.push(dataall[i]);
          }
      });
      this.service.accountuserservice(this.iduserlocal).subscribe(data => {
          const dataall = data.data;
          this.iduserpunyagawai = dataall[0].id;
      });
  }
    onFileChange(event) {
        if (event.target.files.length > 0) {
            let file = event.target.files[0];
            const reader = new FileReader();
            reader.onload = e => this.imageSrc = reader.result;
            reader.readAsDataURL(file);
            this.form.get('avatar').setValue(file);
        }

    }

    prepareSave(): any {
        var input = new FormData();
        input.append('image', this.form.get('avatar').value);
        return input;
    }

    createForm() {
        this.form = this.fb.group({
            avatar: null
        });
    }

    save(category_id, lowongan , bayaran) {
        const formModel = this.prepareSave();
        const image = formModel.get('image');
        const getdate = new Date();
        const dateNow = getdate.toISOString().split('T')[0];
        if (this.selectRadio && lowongan) {
            this.service.useruploadLowongan(category_id, this.selectRadio , lowongan , bayaran , this.iduserpunyagawai , dateNow).subscribe(data => {
                const idLowongan = data.idlowongan;
                if (image !== 'null'){
                    this.service.uploadimageuser(formModel).subscribe(data => {
                        if (data.status === 200) {
                            const urlimage = 'imageuser/image_' + image.name;
                            this.service.uploadImgaLowonganDb(this.iduserlocal , urlimage , idLowongan).subscribe();
                            this.imageSrc = '';
                        }
                    });
                }
                this.showstoryuploadalert = true;
                setTimeout(() => {
                    this.showstoryuploadalert = false;
                }, 3000);
                this.lowongan.nativeElement.value = '';
                this.bayaran.nativeElement.value = '';
            })
        } else if (!this.selectRadio && !lowongan) {
            this.showstoryalert = true;
            this.showstatusalert = true;
            setTimeout(() => {
                this.showstoryalert = false;
                this.showstatusalert = false;
            }, 3000);
        }else if (!this.selectRadio){
            this.showstatusalert = true;
            setTimeout(() => {
                this.showstatusalert = false;
            }, 3000);
        }else if (!lowongan){
            this.showstoryalert = true;
            setTimeout(() => {
                this.showstoryalert = false;
            }, 3000);
        }

    }

    getvalueRadio(event: any) {
        this.selectRadio = event.target.value;
    }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RiwayatPunyagawaiComponent } from './riwayat-punyagawai.component';

describe('RiwayatPunyagawaiComponent', () => {
  let component: RiwayatPunyagawaiComponent;
  let fixture: ComponentFixture<RiwayatPunyagawaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RiwayatPunyagawaiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RiwayatPunyagawaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

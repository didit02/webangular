import {Component, OnInit} from '@angular/core';
import {DataserviceService} from "../../../service/dataservice.service";

@Component({
    selector: 'app-riwayat-punyagawai',
    templateUrl: './riwayat-punyagawai.component.html',
    styleUrls: ['./riwayat-punyagawai.component.scss']
})
export class RiwayatPunyagawaiComponent implements OnInit {


    datedate: any;
    alllowongan = [];
    iduserpunyagawai = localStorage.getItem('idpunyagawai');
    constructor(private service: DataserviceService) {
    }

    ngOnInit() {
        this.service.getalllowonganbyid(this.iduserpunyagawai).subscribe(data => {
            const dataall = data.data;
            for (let i = 0; i < dataall.length; i++ ){
                const date = dataall[i].date;
                const datedb = new Date(date);
                const today = new Date();
                const getdatenow = today.setHours(0, 0, 0, 0);
                const timeDiff = Math.abs(datedb.getTime() - getdatenow);
                const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                if (diffDays === 0) {
                    this.datedate = 'hari ini'
                } else {
                    this.datedate = diffDays + ' hari yang lalu'
                }
                dataall[i]['date_convert'] = this.datedate;
                this.alllowongan.push(dataall[i])
            }
        });
    }

}

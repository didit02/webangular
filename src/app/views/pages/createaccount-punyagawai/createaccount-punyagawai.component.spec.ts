import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateaccountServiceUserComponent } from './createaccount-punyagawai.component';

describe('CreateaccountServiceUserComponent', () => {
  let component: CreateaccountServiceUserComponent;
  let fixture: ComponentFixture<CreateaccountServiceUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateaccountServiceUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateaccountServiceUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {DataserviceService} from "../../../service/dataservice.service";




@Component({
    selector: 'app-createaccount-punyagawai',
    templateUrl: './createaccount-punyagawai.component.html',
    styleUrls: ['./createaccount-punyagawai.component.scss']
})
export class CreateaccountPunyagawaiComponent implements OnInit {

    imageSrc: string;
    form: FormGroup;
    iduser = localStorage.getItem('iduser');

    constructor(private fb: FormBuilder, private service: DataserviceService) {
        this.createForm();
    }

    ngOnInit() {
    }

    onFileChange(event) {
        if (event.target.files.length > 0) {
            let file = event.target.files[0];
            const reader = new FileReader();
            reader.onload = e => this.imageSrc = reader.result;
            reader.readAsDataURL(file);
            this.form.get('avatar').setValue(file);
        }

    }

    prepareSave(): any {
        var input = new FormData();
        input.append('image', this.form.get('avatar').value);
        return input;
    }

    createForm() {
        this.form = this.fb.group({
            avatar: null
        });
    }

    onSubmit(nama_usaha, no_hp_usaha, alamat) {
        const formModel = this.prepareSave();
        const image = formModel.get('image');
        if (image !== 'null') {
            this.service.uploadimageuser(formModel).subscribe(data => {
                if (data.status === 200) {
                    const urlimage = 'imageuser/image_' + image.name;
                    this.service.createuserSerivce(nama_usaha, no_hp_usaha, alamat, this.iduser, urlimage).subscribe();
                    window.location.reload();
                }
            })
        } else {
            alert('image harus diisi')
        }
    }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateStoryComponent } from './create-story.component';
import {expect} from "@angular/core/testing/src/testing_internal";

import {describe} from "selenium-webdriver/testing";

describe('CreateStoryComponent', () => {
  let component: CreateStoryComponent;
  let fixture: ComponentFixture<CreateStoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateStoryComponent ]
    })

        .compileComponents();
  }));

  beforeEach(()=> {
    fixture = TestBed.createComponent(CreateStoryComponent);


    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', ()=> {
    expect(component).toBeTruthy();
  });
});

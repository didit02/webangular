import {DataserviceService} from "../../../service/dataservice.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';

@Component({
    selector: 'app-create-story',
    templateUrl: './create-story.component.html',
    styleUrls: ['./create-story.component.scss']
})
export class CreateStoryComponent implements OnInit {
    form: FormGroup;
    category = [];
    status = [];
    selectRadio: any;
    showstoryalert: boolean;
    showstoryuploadalert: boolean;
    showstatusalert: boolean;
    imageSrc: string;
    iduserlocal = localStorage.getItem('iduser');

    @ViewChild('story') story: ElementRef;

    constructor(private service: DataserviceService , private fb: FormBuilder) {
        this.createForm();
    }

    ngOnInit() {
        this.showstoryalert = false;
        this.showstoryuploadalert = false;
        this.showstatusalert = false;
        this.service.getCategory().subscribe(data => {
            const dataall = data.data;
            for (let i = 0; i < dataall.length; i++) {
                this.category.push(dataall[i]);
            }
        });
        this.service.getStatus().subscribe(data => {
            const dataall = data.data;
            for (let i = 0; i < dataall.length; i++) {
                this.status.push(dataall[i]);
            }
        });
    }

    onFileChange(event) {
        if (event.target.files.length > 0) {
            let file = event.target.files[0];
            const reader = new FileReader();
            reader.onload = e => this.imageSrc = reader.result;
            reader.readAsDataURL(file);
            this.form.get('avatar').setValue(file);
        }

    }

    prepareSave(): any {
        var input = new FormData();
        input.append('image', this.form.get('avatar').value);
        return input;
    }

    createForm() {
        this.form = this.fb.group({
            avatar: null
        });
    }

    save(category_id, story) {
        const formModel = this.prepareSave();
        const image = formModel.get('image');
        const getdate = new Date();
        const dateNow = getdate.toISOString().split('T')[0];
        if (this.selectRadio && story) {
            const iduser = localStorage.getItem('iduser');
            this.service.uploadStory(iduser, category_id, story , dateNow , this.selectRadio).subscribe(data => {
                const idStory = data.idstory;
                if (image !== 'null'){
                    this.service.uploadimageuser(formModel).subscribe(data => {
                        if (data.status === 200) {
                            const urlimage = 'imageuser/image_' + image.name;
                            this.service.uploadImgaStoryDb(this.iduserlocal , urlimage , idStory).subscribe();
                            this.imageSrc = '';
                        }
                    });
                }
                this.showstoryuploadalert = true;
                setTimeout(() => {
                    this.showstoryuploadalert = false;
                }, 3000);
                this.story.nativeElement.value = '';
            })
        } else if (!this.selectRadio && !story) {
            this.showstoryalert = true;
            this.showstatusalert = true;
            setTimeout(() => {
                this.showstoryalert = false;
                this.showstatusalert = false;
            }, 3000);
        }else if (!this.selectRadio){
            this.showstatusalert = true;
            setTimeout(() => {
                this.showstatusalert = false;
            }, 3000);
        }else if (!story){
            this.showstoryalert = true;
            setTimeout(() => {
                this.showstoryalert = false;
            }, 3000);
        }

    }

    getvalueRadio(event: any) {
        this.selectRadio = event.target.value;
    }

}

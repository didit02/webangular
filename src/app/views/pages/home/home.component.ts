import {Component, OnInit} from '@angular/core';
import {DataserviceService} from "../../../service/dataservice.service";


@Component({
    selector: 'app-home', templateUrl: './home.component.html', styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

    constructor(private service: DataserviceService) {
    }

    ngOnInit() {
    }

    searchenterhome(keyword){
        console.log(keyword)
    }

}

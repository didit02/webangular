import {Component, OnInit} from '@angular/core';
import {DataserviceService} from "../../../service/dataservice.service";
import {Router} from "@angular/router";

@Component({
    selector: 'app-signup', templateUrl: './signup.component.html', styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
    show: boolean;
    showpasswordalert : boolean;
    constructor(private service: DataserviceService, private router: Router) {
    }

    ngOnInit() {
        this.show = false;
        this.showpasswordalert = false;
    }

    signup(email, no_hp, password, confirmpassword) {
        const getemail = email;
        if (this.validateEmail(getemail)) {
            if (password === confirmpassword) {
                this.service.sendRegister(email, no_hp, password).subscribe(data => {
                    if (data.success === 'true') {
                        this.router.navigate(['/login'])
                    }
                })
            } else {
                this.showpasswordalert = true;
                setTimeout(() => {
                    this.showpasswordalert = false;
                }, 3000);
            }
        } else {
            this.show = true;
            setTimeout(() => {
                this.show = false
            }, 3000);
        }

    }

    validateEmail(getemail) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(getemail);
    }

    validateEmailUser(email) {
        const getemail = email;
        if (!this.validateEmail(getemail)) {
            this.show = true;
            setTimeout(() => {
                this.show = false
            }, 3000);
        }
    }

    validatePasswordUser(password, confirmpassword) {
            if (password !== confirmpassword) {
                this.showpasswordalert = true;
                setTimeout(() => {
                    this.showpasswordalert = false;
                }, 3000);
            }

    }

}

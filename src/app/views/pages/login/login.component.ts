import {Component, OnInit} from '@angular/core';
import {DataserviceService} from "../../../service/dataservice.service";
import {Router} from "@angular/router";



@Component({
    selector: 'app-login', templateUrl: './login.component.html', styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    emailbinding: string;
    show:boolean;
    showpasswordalert:boolean;
    constructor(private service:DataserviceService , private router:Router) {
    }

    ngOnInit() {
        this.show = false;
        this.showpasswordalert = false
    }

    login(email, sandi) {
        const getemail = email;
        if (this.validateEmail(getemail)){
            this.service.sendLogin(email, sandi).subscribe(data => {
                if (data.success === true) {
                    this.service.setLoggedin(true);
                        this.service.getuserDetail(data.user_id).subscribe(data => {
                            this.router.navigate(['/home']);
                            const dataget = data.data;
                            dataget.forEach(function (item) {
                                localStorage.setItem('iduser', item.id);
                                localStorage.setItem('email', item.email);

                            });
                        });
                }else {
                    this.showpasswordalert = true;
                    setTimeout(() => {
                        this.showpasswordalert = false;
                    }, 3000);
                }
            })
        }else {
            this.show = true;
            setTimeout(() => {
                this.show = false
            }, 3000);
        }
    }


    loginenter(email, sandi){
        const getemail = email;
        if (this.validateEmail(getemail)){
            this.service.sendLogin(email, sandi).subscribe(data => {
                if (data.success === true) {
                    this.service.setLoggedin(true);
                    this.service.getuserDetail(data.user_id).subscribe(data => {
                        this.router.navigate(['/home']);
                        const dataget = data.data;
                        dataget.forEach(function (item) {
                            localStorage.setItem('iduser', item.id);
                            localStorage.setItem('email', item.email);

                        });
                    });
                }else {
                    this.showpasswordalert = true;
                    setTimeout(() => {
                        this.showpasswordalert = false;
                    }, 3000);
                }
            })
        }else {
            this.show = true;
            setTimeout(() => {
                this.show = false
            }, 3000);
        }
    }



    validateEmail(getemail) {
        const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(getemail);
    }

    validateEmailUser(email){
        const getemail = email;
        if (!this.validateEmail(getemail)){
            this.show = true;
            setTimeout(() => {
                this.show = false
            }, 3000);
        }
    }


}

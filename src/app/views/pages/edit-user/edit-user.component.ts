import {Component, ElementRef, OnInit} from '@angular/core';
import {DataserviceService} from "../../../service/dataservice.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
    selector: 'app-edit-user', templateUrl: './edit-user.component.html', styleUrls: ['./edit-user.component.scss']
})
export class EditUserComponent implements OnInit {

    show: boolean;
    form: FormGroup;
    iduser: string = localStorage.getItem('iduser');
    detailuser: any;
    no_hp: any;
    imageSrc: string;
    showedituser:boolean;
    resetpassword: boolean;
    showresetpassword:boolean;

    constructor(private elRef: ElementRef, private service: DataserviceService, private fb: FormBuilder, private router: Router) {
        this.createForm();
    }

    ngOnInit() {
        this.resetpassword = true;
        this.show = false;
        this.showedituser = true;
        this.showresetpassword = false;
        this.service.getuserDetail(this.iduser).subscribe(data => {
            this.detailuser = data.data[0]['email'];
            this.no_hp = data.data[0]['no_hp'];
            this.imageSrc = 'http://localhost:8080/' + data.data[0]['image'];
            console.log(this.imageSrc)
        });

    }

    resetSandi() {
        this.showedituser = false;
        this.showresetpassword = true;
    }

    onFileChange(event) {
        if (event.target.files.length > 0) {
            let file = event.target.files[0];
            const reader = new FileReader();
            reader.onload = e => this.imageSrc = reader.result;
            reader.readAsDataURL(file);
            this.form.get('avatar').setValue(file);
        }

    }

    prepareSave(): any {
        var input = new FormData();
        input.append('image', this.form.get('avatar').value);
        return input;
    }

    onSubmit() {
        const formModel = this.prepareSave();
        const image = formModel.get('image');
        this.service.uploadimageuser(formModel).subscribe(data => {
            if (data.status === 200) {
                const urlimage = 'imageuser/image_' + image.name;
                this.service.uploadImgaDb(urlimage, this.iduser).subscribe();
                window.location.reload();
            }
        })
    }

    createForm() {
        this.form = this.fb.group({
            avatar: null
        });
    }
    cancelresetpassword(){
        this.showedituser = true;
        this.showresetpassword = false;
    }
}

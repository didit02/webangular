import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditPunyagawaiUserComponent } from './edit-punyagawai-user.component';

describe('EditPunyagawaiUserComponent', () => {
  let component: EditPunyagawaiUserComponent;
  let fixture: ComponentFixture<EditPunyagawaiUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditPunyagawaiUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditPunyagawaiUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component,OnInit} from '@angular/core';
import {DataserviceService} from "../../../service/dataservice.service";

import {FormBuilder, FormGroup} from "@angular/forms";
import {Router} from "@angular/router";

@Component({
  selector: 'app-edit-punyagawai-user',
  templateUrl: './edit-punyagawai-user.component.html',
  styleUrls: ['./edit-punyagawai-user.component.scss']
})
export class EditPunyagawaiUserComponent implements OnInit {

    form: FormGroup;
    idpunyagawai: string = localStorage.getItem('idpunyagawai');
    detailpunyagawai: any;
    no_hp: any;
    imageSrc: string;
    showedituser: boolean;
    alamatpunyagawai: string;

    constructor(private service: DataserviceService, private fb: FormBuilder, private router: Router) {
        this.createForm();
    }

    ngOnInit() {
        this.resetpassword = true;
        this.showedituser = true;
        this.showresetpassword = false;
        this.service.getpunyagawaiDetail(this.idpunyagawai).subscribe(data => {
            this.detailpunyagawai = data.data[0]['nama_jasa'];
            this.no_hp = data.data[0]['no_hp_jasa'];
            this.alamatpunyagawai = data.data[0]['alamat'];
            this.imageSrc = 'http://localhost:8080/' + data.data[0]['image'];
        });
    }

    onFileChange(event) {
        if (event.target.files.length > 0) {
            let file = event.target.files[0];
            const reader = new FileReader();
            reader.onload = e => this.imageSrc = reader.result;
            reader.readAsDataURL(file);
            this.form.get('avatar').setValue(file);
        }

    }

    prepareSave(): any {
        var input = new FormData();
        input.append('image', this.form.get('avatar').value);
        return input;
    }

    onSubmit() {
        const formModel = this.prepareSave();
        const image = formModel.get('image');
        this.service.uploadimageuser(formModel).subscribe(data => {
            if (data.status === 200) {
                const urlimage = 'imageuser/image_' + image.name;
                this.service.uploadImgaDb(urlimage, this.iduser).subscribe();
                window.location.reload();
            }
        })
    }

    createForm() {
        this.form = this.fb.group({
            avatar: null
        });

    }
}

import {Component, OnInit} from '@angular/core';
import {DataserviceService} from "../../../service/dataservice.service";

@Component({
    selector: 'app-my-story', templateUrl: './my-story.component.html', styleUrls: ['./my-story.component.scss']
})
export class MyStoryComponent implements OnInit {
    datedate: any;
    mystory = [];
    constructor(private service: DataserviceService) {
    }

    ngOnInit() {
        const iduser = localStorage.getItem('iduser');
        this.service.mystory(iduser).subscribe(data =>{
            const dataall = data.data;
            for (let i = 0; i < dataall.length; i++ ){
                const date = dataall[i].date;
                const datedb = new Date(date);
                const today = new Date();
                const getdatenow = today.setHours(0, 0, 0, 0);
                const timeDiff = Math.abs(datedb.getTime() - getdatenow);
                const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                if (diffDays === 0) {
                    this.datedate = 'hari ini'
                } else {
                    this.datedate = diffDays + ' hari yang lalu'
                }
                dataall[i]['date_convert'] = this.datedate;
                this.mystory.push(dataall[i])
            }

        })
    }

}

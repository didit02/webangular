import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HowShareComponent } from './how-share.component';

describe('HowShareComponent', () => {
  let component: HowShareComponent;
  let fixture: ComponentFixture<HowShareComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HowShareComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HowShareComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GawaiComponent } from './gawai.component';

describe('GawaiComponent', () => {
  let component: GawaiComponent;
  let fixture: ComponentFixture<GawaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GawaiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GawaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

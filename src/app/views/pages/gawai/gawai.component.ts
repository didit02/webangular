import {Component, OnInit} from '@angular/core';
import {DataserviceService} from "../../../service/dataservice.service";

import {Router} from "@angular/router";

@Component({
    selector: 'app-gawai', templateUrl: './gawai.component.html', styleUrls: ['./gawai.component.scss']
})
export class GawaiComponent implements OnInit {

    datedate: any;
    alllowongan = [];
    payshow:boolean;
    iduser = localStorage.getItem('iduser');
    constructor(private service: DataserviceService , private router:Router) {
    }

    ngOnInit() {
        this.payshow = this.iduser !== null;
        this.service.getalllowongan().subscribe(data =>{
            const dataall = data.data;
            for (let i = 0; i < dataall.length; i++ ){
                const date = dataall[i].date;
                const datedb = new Date(date);
                const today = new Date();
                const getdatenow = today.setHours(0, 0, 0, 0);
                const timeDiff = Math.abs(datedb.getTime() - getdatenow);
                const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                if (diffDays === 0) {
                    this.datedate = 'hari ini'
                } else {
                    this.datedate = diffDays + ' hari yang lalu'
                }
                dataall[i]['date_convert'] = this.datedate;
                this.alllowongan.push(dataall[i])
            }

        })
    }

    check(event): void {
        console.log('event', event)
    }

    detailgawai(id){
        this.router.navigate(['/detailgawai', id]);
    }
}

import {Component, OnInit} from '@angular/core';
import {DataserviceService} from "../../../service/dataservice.service";


@Component({
    selector: 'app-dashboard-user',
    templateUrl: './dashboard-user.component.html',
    styleUrls: ['./dashboard-user.component.scss']
})
export class DashboardUserComponent implements OnInit {

    iduser = localStorage.getItem('iduser');
    emaillogged = localStorage.getItem('email');

    imageuser: string;
    nameaccountuserservice: string;
    imageaccountuserservice: string;
    addressUsaha: string;
    useraccountService: any;

    buatPunyaGawai: boolean;
    showprofilesaya: boolean;
    showpunyagawai: boolean;

    historyaccountuser: boolean;

    historypunyagawai: boolean;
    UserUploadLowongan: boolean;
    SettingAccountUser: boolean;
    settingaccountPunyaGawai: boolean;


    constructor(private service: DataserviceService) {
    }

    ngOnInit() {
        this.showprofilesaya = true;
        this.showpunyagawai = true;
        this.buatPunyaGawai = true;
        this.service.getImageUser(this.iduser).subscribe(data => {
            const dataall = data.data;
            if (dataall[0].image !== '') {
                this.imageuser = 'http://localhost:8080/' + dataall[0].image;
            } else {
                this.imageuser = '../../../../assets/image/default/no_image.png';
            }

        });

        this.service.accountuserservice(this.iduser).subscribe(data => {
            const dataall = data.data;
            localStorage.setItem('idpunyagawai', dataall[0].user_id);
            if (dataall.length !== 0) {
                this.nameaccountuserservice = dataall[0].nama_jasa;
                this.addressUsaha = dataall[0].alamat;
                this.imageaccountuserservice = 'http://localhost:8080/' + dataall[0].image;
                this.useraccountService = dataall[0] !== "";
                this.buatPunyaGawai = false;
                this.historyaccountuser = false;
                this.historypunyagawai = true;
            } else {
                this.buatPunyaGawai = true;
                this.historyaccountuser = true;
                this.historypunyagawai = false;
            }
        });
    }

    trigerprofilesaya() {
        this.showprofilesaya = this.showprofilesaya != true;
    }

    trigerpunyagawai() {
        this.showpunyagawai = this.showpunyagawai != true;
    }
    routerriwayat() {
        this.settingaccountPunyaGawai = false;
        this.historyaccountuser = true;
        this.historypunyagawai = false;
        this.UserUploadLowongan = false;
        this.UserUploadLowongan = false;
    }

    routersettingprofile() {
        this.settingaccountPunyaGawai = false;
        this.historyaccountuser = false;
        this.historypunyagawai = false;
        this.UserUploadLowongan = false;
        this.SettingAccountUser = true;
    }

    routerriwayapostingan() {
        this.settingaccountPunyaGawai = false;
        this.historyaccountuser = false;
        this.historypunyagawai = true;
        this.UserUploadLowongan = false;
        this.SettingAccountUser = false;

    }

    routersettingpunyagawai() {
        this.settingaccountPunyaGawai = true;
        this.historyaccountuser = false;
        this.historypunyagawai = false;
        this.UserUploadLowongan = false;
        this.SettingAccountUser = false;
    }

    routerpostinglowongan() {
        this.settingaccountPunyaGawai = false;
        this.historyaccountuser = false;
        this.historypunyagawai = false;
        this.UserUploadLowongan = true;
        this.SettingAccountUser = false;
    }


}

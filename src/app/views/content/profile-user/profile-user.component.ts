import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {DataserviceService} from "../../../service/dataservice.service";


@Component({
    selector: 'app-profile-user',
    templateUrl: './profile-user.component.html',
    styleUrls: ['./profile-user.component.scss']
})
export class ProfileUserComponent implements OnInit {

    logged: boolean;
    emaillogged: string;
    emailsplit: string;
    imageuser:string;
    show: boolean;

    constructor(private router: Router, private service: DataserviceService) {
    }

    ngOnInit() {
        this.service.setLoggedin(true);
        this.show = false;
        const emaillocalStorage = localStorage.getItem('email');
        const getiduser = localStorage.getItem('iduser');
        this.logged = emaillocalStorage !== null ? false : true;
        this.emaillogged = emaillocalStorage;
        this.emailsplit = emaillocalStorage.split('@')[0];
        this.service.getImageUser(getiduser).subscribe(data =>{
            const  dataall = data.data;
            if (dataall[0].image !== '') {
                this.imageuser = 'http://localhost:8080/' + dataall[0].image;
            }else {
                this.imageuser = '../../../../assets/image/default/no_image.png';
            }

        })
    }

    logout(): void {
        localStorage.removeItem('email');
        localStorage.removeItem('iduser');
        localStorage.removeItem('idpunyagawai');
        if (this.router.url === '/home'){
            window.location.reload()
        }else {
            this.router.navigateByUrl('/home');
        }
    }

    dropdownuser() {
        this.show = this.show == false;
    }

    editUser() {
        this.show = false;
        this.router.navigateByUrl('/edit_user');
    }

    createStory() {
        this.show = false;
        this.router.navigateByUrl('/create_story');
    }

    myStory() {
        this.show = false;
        this.router.navigateByUrl('/my_story');
    }
    mouseOverdrop(){
        this.show = false;
    }
}

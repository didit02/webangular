import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserUploadJasaComponent } from './user-upload-lowongan.component';

describe('UserUploadJasaComponent', () => {
  let component: UserUploadJasaComponent;
  let fixture: ComponentFixture<UserUploadJasaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserUploadJasaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserUploadJasaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

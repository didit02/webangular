import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryPunyagawaiComponent } from './history-punyagawai.component';

describe('HistoryPunyagawaiComponent', () => {
  let component: HistoryPunyagawaiComponent;
  let fixture: ComponentFixture<HistoryPunyagawaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoryPunyagawaiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryPunyagawaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

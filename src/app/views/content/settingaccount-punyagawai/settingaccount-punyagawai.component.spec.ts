import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingaccountPunyagawaiComponent } from './settingaccount-punyagawai.component';

describe('SettingaccountPunyagawaiComponent', () => {
  let component: SettingaccountPunyagawaiComponent;
  let fixture: ComponentFixture<SettingaccountPunyagawaiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingaccountPunyagawaiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingaccountPunyagawaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

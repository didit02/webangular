import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingaccountUserComponent } from './settingaccount-user.component';

describe('SettingaccountUserComponent', () => {
  let component: SettingaccountUserComponent;
  let fixture: ComponentFixture<SettingaccountUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingaccountUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingaccountUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

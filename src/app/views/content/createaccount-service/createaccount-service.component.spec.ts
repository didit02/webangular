import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateaccountServiceComponent } from './createaccount-service.component';

describe('CreateaccountServiceComponent', () => {
  let component: CreateaccountServiceComponent;
  let fixture: ComponentFixture<CreateaccountServiceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateaccountServiceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateaccountServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
